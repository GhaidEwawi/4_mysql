import mysql.connector
import random
import string
import pandas as pd
from mysql.connector import Error
import datetime
from faker import Faker
import numpy as np
fake = Faker()


# Functions:
def randomString(stringLength=8):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

def getFields(table_name):
    if table_name == 'tickets':
        return '(`passenger_id`, `flight_id`)', 2
    elif table_name == 'pilots':
        return '(`name`, `starting_date`, `salary`, `country`, `email`, `phone_no`)', 6
    elif table_name == 'aircrafts':
        return '(`model`, `capacity`)', 2
    elif table_name == 'flights':
        return '(`pilot_id`, `aircraft_id`, `_date`, `_time`, `destination`)', 5
    else:
        return '(`country`, `name`, `email`, `phone_no`)', 4

def generateInsertSql(table_name):
    return f"INSERT INTO `{table_name}` {getFields(table_name)[0]} VALUES (%s{', %s' * (getFields(table_name)[1] -1)})"

def selectValues(table_name, size=5):
    if table_name == 'tickets':
        return addTickets(size)
    elif table_name == 'pilots':
        return addPilots(size)
    elif table_name == 'aircrafts':
        return addAircrafts(size)
    elif table_name == 'flights':
        return addFlights(size)
    else:
        return addPassengers(size)

def addAircrafts(size=5):
    aircrafts = []
    for i in range(size):
        aircraft = {}
        model = randomString(4)
        capacity = random.randint(100, 400)
        aircrafts.append((model, capacity))
    return aircrafts

def addFlights(size=5):
    flights = []
    for i in range(size):
        pilot_id = random.randint(0, 8)
        aircraft_id = random.randint(0, 8)
        _date = randomDate()
        _time = randomTime()
        destination = fake.country()
        flights.append((pilot_id, aircraft_id, _date, _time, destination))
    return flights

def addPassengers(size=5):
    passengers = []
    for i in range(size):
        country = fake.country()
        name = fake.name()
        email = fake.email()
        number = fake.phone_number()
        passengers.append((country, name, email, number))

    return passengers

def addPilots(size=5):
    pilots = []
    for i in range(size):
        name = fake.name()
        _date = randomDate()
        salary = random.randint(4000, 5000)
        country = fake.country()
        email = fake.email()
        phone_no = fake.phone_number()
        pilots.append((name, _date, salary, country, email, phone_no))

    return pilots

def addTickets(size=5):
    tickets = []
    for i in range(size):
        passenger_id = random.randint(1, 13)
        flight_id = random.randint(1, 13)
        tickets.append((passenger_id, flight_id))
    return tickets

def insertIntoTable(table_name, size=5):

    sql = generateInsertSql(table_name) # "INSERT INTO customers (name, address) VALUES (%s, %s)"
    val = selectValues(table_name, size)

    cursor.executemany(sql, val)
    mydb.commit()
    print(cursor.rowcount, "was inserted.")

def randomTime():
    minuteList = [0, 15, 30, 45]
    hours = random.randint(0, 23)
    minutes = minuteList[random.randint(0, 3)]
    return f'{hours}:{minutes}:00'

def randomDate():
    start_date = datetime.date(2020, 1, 1)
    end_date = datetime.date(2021, 2, 1)

    time_between_dates = end_date - start_date
    days_between_dates = time_between_dates.days
    random_number_of_days = random.randrange(days_between_dates)
    random_date = start_date + datetime.timedelta(days=random_number_of_days)
    return random_date

def getData():
    aircrafts = pd.read_sql('SELECT * FROM aircrafts', mydb)
    flights = pd.read_sql('SELECT * FROM flights', mydb)
    passengers = pd.read_sql('SELECT * FROM passengers', mydb)
    pilots = pd.read_sql('SELECT * FROM pilots', mydb)
    tickets = pd.read_sql('SELECT * FROM tickets', mydb)
    return aircrafts, flights, passengers, pilots, tickets

def queryA():
    result = flights.merge(aircrafts, on='aircraft_id')
    result['_date'] = pd.to_datetime(result['_date'])
    print('Print the flight number, number of passengers, and flight date for all flights that took place in May 2020.')
    print(result[(result['_date'].dt.year == 2020) & (result['_date'].dt.month == 5)][['flight_id', 'capacity', '_date']])

def queryB():
    print('Print the flight info for all flights between our airport and London.')
    print(flights[flights['destination'] == 'London'])

def queryC():
    print('Print the total number of passengers who used the airport during 2020.')
    result = passengers.merge(tickets, on='passenger_id').merge(flights, on='flight_id')
    result['_date'] = pd.to_datetime(result['_date'])
    print(len(result[result['_date'].dt.year == 2020]))

def queryD():
    print('Print the aircraft name, number of passengers for all aircrafts in the system, ordered descending by number of passengers.')
    result = aircrafts.sort_values('capacity', ascending=False)
    print(result)

def queryE():
    print('Print the names of all Palestinian passengers who arrived at the airport.')
    print(passengers[passengers['country'] == 'Palestine']['name'])

def queryF():
    print('Print the country of residence, number of flights, and average number of passengers per flight for all countries.')
    temp = pd.DataFrame()
    result = flights.merge(tickets, on='flight_id')
    result.groupby('destination')['flight_id'].agg(np.mean)
    temp['flights number'] = flights['destination'].value_counts()
    temp['average passengers'] = result.groupby('destination')['flight_id'].agg(np.mean)
    print(temp)

def queryG():
    print('Print the full information of the top 5 travelling passengers.')
    result = tickets
    temp = pd.DataFrame()
    temp['passenger_id'] = result['passenger_id'].value_counts().index
    print(temp.merge(passengers, on='passenger_id').iloc[:5, :])


# Main function
if __name__ == "__main__":
    mydb = mysql.connector.connect(
      host="localhost",
      user="root",
      password="",
      database="airport"
    )

    try:
        if mydb.is_connected():
            db_Info = mydb.get_server_info()
            print("Connected to MySQL Server version ", db_Info)
            cursor = mydb.cursor()
            cursor.execute("select database();")
            record = cursor.fetchone()
            print("You're connected to database: ", record)
            #insertIntoTable('tickets', 15)

            aircrafts, flights, passengers, pilots, tickets = getData()

            # queryA()
            # queryB()
            # queryC()
            # queryD()
            # queryE()
            # queryG()

    except Error as e:
        print("Error while connecting to MySQL", e)
    finally:
        print('HI')
        if (mydb.is_connected()):
            cursor.close()
            mydb.close()
            print("MySQL connection is closed")
