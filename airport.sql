-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 05, 2020 at 10:22 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `airport`
--

-- --------------------------------------------------------

--
-- Table structure for table `aircrafts`
--

DROP TABLE IF EXISTS `aircrafts`;
CREATE TABLE IF NOT EXISTS `aircrafts` (
  `aircraft_id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(30) NOT NULL,
  `capacity` int(11) NOT NULL,
  PRIMARY KEY (`aircraft_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aircrafts`
--

INSERT INTO `aircrafts` (`aircraft_id`, `model`, `capacity`) VALUES
(1, '777', 440),
(2, 'cghd', 111),
(3, 'covs', 145),
(4, 'slxd', 343),
(5, 'bguz', 364),
(6, 'hdfd', 182);

-- --------------------------------------------------------

--
-- Table structure for table `flights`
--

DROP TABLE IF EXISTS `flights`;
CREATE TABLE IF NOT EXISTS `flights` (
  `flight_id` int(11) NOT NULL AUTO_INCREMENT,
  `pilot_id` int(11) NOT NULL,
  `aircraft_id` int(11) NOT NULL,
  `_date` date NOT NULL,
  `_time` time NOT NULL,
  `destination` varchar(50) NOT NULL,
  PRIMARY KEY (`flight_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `flights`
--

INSERT INTO `flights` (`flight_id`, `pilot_id`, `aircraft_id`, `_date`, `_time`, `destination`) VALUES
(1, 1, 1, '2020-07-15', '08:00:00', 'London'),
(2, 2, 2, '2020-07-06', '06:00:00', 'Australia'),
(3, 5, 8, '2020-11-01', '09:30:00', 'Indonesia'),
(4, 2, 7, '2021-01-14', '05:15:00', 'Dominican Republic'),
(5, 5, 1, '2020-09-22', '13:00:00', 'Lao People\'s Democratic Republic'),
(6, 8, 3, '2021-01-12', '17:00:00', 'United States Minor Outlying Islands'),
(7, 2, 0, '2020-06-14', '17:45:00', 'Bahrain'),
(8, 6, 4, '2020-01-31', '04:15:00', 'Liechtenstein'),
(9, 8, 4, '2020-03-06', '01:45:00', 'Slovakia (Slovak Republic)'),
(10, 1, 6, '2020-05-09', '08:30:00', 'Paraguay'),
(11, 6, 7, '2020-11-22', '00:30:00', 'Indonesia'),
(12, 0, 0, '2020-12-12', '12:30:00', 'Western Sahara'),
(13, 1, 1, '2020-07-14', '13:00:00', 'London');

-- --------------------------------------------------------

--
-- Table structure for table `passengers`
--

DROP TABLE IF EXISTS `passengers`;
CREATE TABLE IF NOT EXISTS `passengers` (
  `passenger_id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(30) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(70) NOT NULL,
  `phone_no` varchar(30) NOT NULL,
  PRIMARY KEY (`passenger_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `passengers`
--

INSERT INTO `passengers` (`passenger_id`, `country`, `name`, `email`, `phone_no`) VALUES
(2, 'Iran', 'Kenneth Trevino', 'michael90@kelly-miller.com', '+1-321-329-5159'),
(3, 'Guadeloupe', 'Benjamin Moran', 'jonescarolyn@hotmail.com', '268.527.4276'),
(4, 'Pakistan', 'Michelle Smith', 'joshuaharmon@taylor-barron.com', '(734)359-2201'),
(5, 'Madagascar', 'Joseph Smith', 'deborahhoward@harding.com', '+1-898-918-8501x85690'),
(6, 'Mali', 'Matthew Pierce', 'angel50@gmail.com', '001-781-434-2580'),
(7, 'Korea', 'David Conley', 'garyhess@yahoo.com', '+1-582-101-6479x2866'),
(8, 'United Arab Emirates', 'Timothy Campbell', 'karen34@ruiz-beard.com', '+1-625-061-8856x8256'),
(9, 'Equatorial Guinea', 'Amanda Johnson', 'ochoanatasha@gmail.com', '940-051-6764'),
(10, 'Cook Islands', 'Sarah Robinson', 'johnmartin@hotmail.com', '001-187-398-4519x54110'),
(11, 'Northern Mariana Islands', 'Timothy Keller', 'elizabethmclaughlin@morse-bell.com', '068-980-7369'),
(12, 'Azerbaijan', 'Timothy Alexander', 'mgallegos@johnston.com', '022-897-0688x739'),
(13, 'Comoros', 'John Wilson', 'iclayton@hotmail.com', '001-272-720-2670x99130'),
(14, 'Palestine', 'Veronica Alexander', 'joshuawalker@gmail.com', '512-580-3477x0139'),
(15, 'Palestine', 'Mr. James Wilson', 'swilliams@quinn.com', '969.123.8130'),
(16, 'Palestine', 'John Kelly', 'crodriguez@yahoo.com', '+1-262-745-9374'),
(17, 'Palestine', 'Christine Herman', 'apotter@yahoo.com', '(798)106-4403'),
(18, 'Palestine', 'Teresa Martinez', 'noah18@martinez.biz', '803-391-1023');

-- --------------------------------------------------------

--
-- Table structure for table `pilots`
--

DROP TABLE IF EXISTS `pilots`;
CREATE TABLE IF NOT EXISTS `pilots` (
  `pilot_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `starting_date` date NOT NULL,
  `salary` int(11) NOT NULL,
  `country` varchar(30) NOT NULL,
  `email` varchar(70) NOT NULL,
  `phone_no` varchar(30) NOT NULL,
  PRIMARY KEY (`pilot_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pilots`
--

INSERT INTO `pilots` (`pilot_id`, `name`, `starting_date`, `salary`, `country`, `email`, `phone_no`) VALUES
(2, 'Stephen Watkins', '2020-05-03', 4908, 'Germany', 'wolfemichael@gmail.com', '912.983.3712x652'),
(3, 'Crystal White', '2020-07-21', 4857, 'Algeria', 'christophermckay@gmail.com', '715-937-2055x917'),
(4, 'Mr. Timothy Conley Jr.', '2020-09-21', 4883, 'Tokelau', 'sabrinagibson@yahoo.com', '269.936.2376'),
(5, 'Lance Warner', '2020-06-15', 4887, 'Cambodia', 'ashley30@brooks.com', '173.527.2155x75475'),
(6, 'Nathan Love Jr.', '2020-02-14', 4958, 'Dominican Republic', 'stephanie38@serrano-strickland.info', '001-509-770-4053');

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

DROP TABLE IF EXISTS `tickets`;
CREATE TABLE IF NOT EXISTS `tickets` (
  `ticket_id` int(11) NOT NULL AUTO_INCREMENT,
  `passenger_id` int(11) NOT NULL,
  `flight_id` int(11) NOT NULL,
  PRIMARY KEY (`ticket_id`)
) ENGINE=MyISAM AUTO_INCREMENT=148 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`ticket_id`, `passenger_id`, `flight_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 1, 1),
(4, 2, 2),
(5, 2, 1),
(6, 3, 1),
(7, 1, 1),
(8, 2, 2),
(9, 2, 1),
(10, 3, 1),
(11, 1, 1),
(12, 2, 2),
(13, 2, 1),
(14, 3, 1),
(15, 1, 1),
(16, 2, 2),
(17, 2, 1),
(18, 3, 1),
(19, 2, 1),
(20, 7, 2),
(21, 8, 7),
(22, 3, 2),
(23, 5, 5),
(74, 12, 5),
(25, 1, 7),
(73, 9, 4),
(27, 2, 5),
(28, 2, 1),
(88, 12, 9),
(30, 5, 6),
(31, 2, 3),
(32, 6, 4),
(33, 7, 2),
(34, 0, 7),
(35, 5, 8),
(36, 6, 4),
(37, 8, 6),
(38, 7, 3),
(39, 1, 1),
(40, 2, 2),
(41, 2, 1),
(42, 3, 1),
(43, 10, 12),
(44, 10, 7),
(45, 2, 10),
(46, 11, 1),
(47, 3, 10),
(48, 8, 1),
(49, 12, 4),
(50, 1, 8),
(51, 5, 2),
(52, 7, 1),
(53, 1, 12),
(54, 5, 13),
(55, 7, 2),
(56, 2, 8),
(57, 3, 3),
(58, 2, 2),
(59, 11, 9),
(60, 10, 4),
(61, 4, 3),
(62, 11, 13),
(63, 13, 5),
(64, 9, 3),
(65, 10, 8),
(66, 13, 2),
(67, 13, 1),
(68, 10, 4),
(69, 2, 1),
(70, 6, 8),
(71, 1, 3),
(72, 9, 6),
(75, 3, 2),
(76, 13, 9),
(77, 7, 9),
(78, 10, 8),
(79, 12, 7),
(80, 6, 6),
(81, 12, 10),
(82, 8, 10),
(83, 13, 8),
(84, 5, 8),
(85, 4, 10),
(86, 12, 10),
(87, 8, 5),
(89, 6, 12),
(90, 1, 13),
(91, 7, 7),
(92, 9, 5),
(93, 10, 2),
(94, 7, 7),
(95, 1, 2),
(96, 10, 7),
(97, 12, 12),
(98, 1, 9),
(99, 3, 9),
(100, 7, 6),
(101, 4, 3),
(102, 3, 9),
(103, 1, 12),
(104, 12, 11),
(105, 9, 13),
(106, 13, 6),
(107, 10, 6),
(108, 7, 9),
(109, 10, 1),
(110, 3, 9),
(111, 12, 13),
(112, 10, 9),
(113, 5, 9),
(114, 13, 3),
(115, 10, 3),
(116, 2, 11),
(117, 13, 5),
(118, 11, 13),
(119, 7, 13),
(120, 9, 6),
(121, 13, 5),
(122, 8, 7),
(123, 13, 1),
(124, 1, 13),
(125, 12, 9),
(126, 12, 12),
(127, 4, 11),
(128, 5, 5),
(129, 5, 2),
(130, 11, 6),
(131, 1, 13),
(132, 7, 7),
(133, 10, 8),
(134, 3, 12),
(135, 8, 13),
(136, 9, 8),
(137, 9, 7),
(138, 2, 5),
(139, 10, 8),
(140, 7, 10),
(141, 11, 4),
(142, 2, 6),
(143, 6, 4),
(144, 11, 13),
(145, 9, 2),
(146, 7, 10),
(147, 4, 7);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
